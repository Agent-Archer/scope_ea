import React from "react";
import {observer, inject} from "mobx-react";
import CustomMap from "./CustomMap";
import UserList from "./UserList";
import VehicleList from "./VehicleList";

@inject("MainStore")
@observer
class App extends React.Component {
    // constructor(props) {
    //     super(props);       
    // };

    render() { 
        //const mainStore = this.props.MainStore;
    
        return (
            <div className="container">            
               { <UserList /> }
               { <VehicleList /> } 
               { <CustomMap /> }                         
            </div>            
        );
    }
}

export default App