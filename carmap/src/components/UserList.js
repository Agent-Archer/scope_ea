import React from "react";
import {inject, observer} from 'mobx-react';
import {toJS} from 'mobx';

@inject("UserStore")
@inject("MapStore")
@observer
class UserList extends React.Component {
    componentDidMount() {
      this.props.UserStore.getUsersAsync();
    }
    
    render() { 
      const mapStore = this.props.MapStore; 

      const data = toJS(this.props.UserStore.userData) ? 
        toJS(this.props.UserStore.userData).filter(function (el) {
          return el.userid !== undefined;
        }) : null;

      return (
        <div className="container bootstrap snippet user-list-container"  style={{float: "left", width: "60%" }}>
          <div className="row">
                  <div className="main-box no-header clearfix">
                      <div className="main-box-body clearfix">
                          <div className="table-responsive"></div>
                            <table className="table user-list">
                                            <thead>
                                                  <tr>
                                                    <th><span>Id</span></th>
                                                    <th><span>User</span></th>
                                                    <th><span>Name</span></th>
                                                    <th><span>Surname</span></th>
                                                    <th><span>Vehicles</span></th>                                                
                                                  </tr>
                                              </thead>
                                              <tbody>
                                            {data && data.map(item =>                                            
                                    
                                                <tr key={item.userid} onClick={(e) => mapStore.userSelect(item)}>
                                                 <td style={{width: "50px"}}>
                                                  {item.userid}
                                                </td>
                                                <td>
                                                  {item.owner ? <img src={item.owner.foto} alt="" /> : null}
                                                </td>
                                                <td>  
                                                  {item.owner ? item.owner.name : null}
                                                </td>                                              
                                                <td>
                                                  { item.owner ? item.owner.surname : null}
                                                </td>            
                                                <td>                                                  
                                                  { item.vehicles ?  Object.keys(item.vehicles).length : 0 }
                                                </td>
                                              </tr>
                                            )}
                              </tbody>
                            </table>
                    </div>
                </div>
              
            </div>
        </div>
      );
  }
}

export default UserList;