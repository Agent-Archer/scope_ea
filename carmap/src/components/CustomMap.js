import React  from "react";
import {inject, observer} from 'mobx-react';
import {toJS} from 'mobx';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';

@inject("MapStore")
@observer
class CustomMap extends React.Component { 
  
  onMarkerClick = (props, marker, e) => {
    this.props.MapStore.onMarkerClick(marker);  
  };

   render() { 
    const mapStore = this.props.MapStore;
    const vehicles = toJS(mapStore.vehicleLocations);
    const geoCode = new window.GeoCode(); 
   
    return (
        <Map center={mapStore.initCenter} zoom={13}>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
            {vehicles && vehicles.map((place) => {              

              if(place.lat && mapStore) {
                var vech = toJS(mapStore.vehicles.filter(function (el) {
                    return el.vehicleid === place.vehicleid; }))[0];
                  
                  geoCode.reverse(place.lat, place.lon).then(result => {
                     mapStore.addressObj = result;                   
                  });
                
                  return(                    
                      <Marker  key={place.vehicleid} position={[place.lat, place.lon]} 
                          onClick={() => { mapStore.selectedVehicleId = place.vehicleid} }>
                          <Popup> 
                              <div>
                                {vech ? vech.make : null}, {vech ? vech.model : null}, {vech ? vech.year : null}
                              </div>
                              <div>
                               Address : { mapStore.addressObj ? mapStore.addressObj.address : null }
                              </div>                              
                              <img width="150px" height="150px" src={vech ? vech.foto : null} alt="" />
                          </Popup>
                      </Marker>
                  );}
            })}            
          </Map> 
    );
  }
}
export default CustomMap;