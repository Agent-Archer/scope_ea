import React from "react";
import {inject, observer} from 'mobx-react';
import {toJS} from 'mobx';

@inject("MapStore")
@observer
class VehicleList extends React.Component {
    
    render() { 
      const mapStore = this.props.MapStore; 

      const data = toJS(mapStore.vehicles) ? 
        toJS(mapStore.vehicles) : null;

      return (
        <div className="container bootstrap snippet vehicle-container" style={{ float: "right", width: "35%" }}>
          <div className="row">              
                  <div className="main-box no-header clearfix">
                      <div className="main-box-body clearfix">
                          <div className="table-responsive"></div>
                            <table className="table">
                                            <thead>
                                                  <tr>
                                                    <th><span>Id</span></th>
                                                    <th><span>Make</span></th>
                                                    <th><span>Model</span></th>
                                                    <th><span>Year</span></th>
                                                    <th><span>VIN</span></th>                                                                                                   
                                                  </tr>
                                              </thead>
                                              <tbody>
                                            {data && data.map(item =>  
                                                <tr key={item.vehicleid} 
                                                 className={item.vehicleid === mapStore.selectedVehicleId ? 'vehicle-list' : ''}>
                                                    <td style={{width: "50px"}}>
                                                    {item.vehicleid}
                                                    </td>
                                                    <td>
                                                    {item.make ? item.make : null}
                                                    </td>
                                                    <td>  
                                                    {item.model ? item.model : null}
                                                    </td>                                              
                                                    <td>
                                                    { item.year ? item.year : null}
                                                    </td>
                                                    <td>
                                                    { item.vin ? item.vin : null}
                                                    </td>
                                              </tr>
                                            )}
                              </tbody>
                            </table>
                    </div>
                </div>
              
            </div>
        </div>
      );
  }
}

export default VehicleList;