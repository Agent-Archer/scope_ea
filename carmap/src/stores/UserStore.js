import { observable, runInAction, action, toJS } from 'mobx';
import CarService from '../services/CarService'

export default class UserStore {
    constructor() {
        this.carService = new CarService();
    }

    @observable userData = [];
    @observable searchQuery = "";

    @action getUsersAsync = async () => {
        try {
            var params = {                
                searchQuery: this.searchQuery               
            };
            const urlParams = new URLSearchParams(Object.entries(params));
            const data = await this.carService.get(urlParams)
            runInAction(() => {                                
                this.userData = toJS(data).data; 
            });
        } catch (error) {
            runInAction(() => {
                alert("Sorry server error accured. Could you please try one more time.")
                console.log(error);
            });
        }
    };  
}