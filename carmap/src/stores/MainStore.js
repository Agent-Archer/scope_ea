import MapStore from "./MapStore";
import UserStore from "./UserStore"

class MainStore {
    constructor() {
        this.MapStore = new MapStore();  
        this.UserStore = new UserStore(); 
    } 
}

export default new MainStore();