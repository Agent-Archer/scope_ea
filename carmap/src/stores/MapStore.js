import { observable, runInAction, action, toJS, computed } from 'mobx';
import CarService from '../services/CarService';


export default class MapStore {
    constructor(props){
        this.carService = new CarService();
    }

    addressObj = {};
    @observable selectedVehicleId = -1;

    @observable vehicles = [];  
    @observable vehicleLocations = [];
    
    @computed get initCenter()  {
        return { lat: this.vehicleLocations[0] ? this.vehicleLocations[0].lat : 10, 
            lng: this.vehicleLocations[0] ? this.vehicleLocations[0].lon : 10 };
    };

    @action userSelect(user) {       
        this.vehicles = user.vehicles;
        this.getLocationdata(user.userid);       
    }

   getLocationdata = async (userId) => {
        try {
            const urlParams = new URLSearchParams([["op", "getlocations"],["userid", userId]]);
            const data = await this.carService.get(urlParams)
            runInAction(() => {                                
                this.vehicleLocations = toJS(data).data; 
            });
        } catch (error) {
            runInAction(() => {
                alert("Sorry server error accured. Could you please try one more time.")
                console.log(error);
            });
        }
    };
}