import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "mobx-react";
import App from "./components/App";
import MainStore from "./stores/MainStore";
import './index.css';

// IE11
// require("es6-object-assign").polyfill();
// require( "./static/less/main.less");

const stores = {
    MainStore,    
    MapStore : MainStore.MapStore,
    UserStore : MainStore.UserStore
};

ReactDOM.render((
    <Provider {...stores}>
        <App />
    </Provider>
), document.getElementById('root'));